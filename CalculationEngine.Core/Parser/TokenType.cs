﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CalculationEngine.Core.Parser
{
    internal enum TokenType
    {
        NoToken,
        Comment,
        FakeNumber,
        FakeBinaryOperator,
        FakeLeftParentheses,
        FakeRightParentheses,
        Number,
        Variable,
        Index,
        Equal,
        Plus,
        Minus,
        Times,
        Divide,
        Differencial,
        Derivative,
        Parentheses,
        LeftParentheses,
        RightParentheses,
        Symbol,
        Exponent,
        Root,
        SquareRoot,
        Delta,
        BooleanEqual,
        BooleanLess,
        BooleanLessEqual,
        BooleanMore,
        BooleanMoreEqual,
        BooleanAnd,
        BooleanOr,
        If,
        InvertedIf,
        Else,
        Equations,
        EquationStart,
        EquationEnd,
        EquationUsage,
        EquationNew,
        Pi,
        E,
        NaN,
        Separator
    }
}
