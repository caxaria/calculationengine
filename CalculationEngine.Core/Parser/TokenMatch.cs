﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CalculationEngine.Core.Parser
{
    internal class TokenMatch
    {
        internal TokenType Type { get; set; }
        internal String Text { get; set; }
        internal int Start { get; set; }
        internal int End { get; set; }
    }
}
