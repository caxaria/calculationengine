﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculationEngine.Core.Parser;
using System.Text.RegularExpressions;

namespace CalculationEngine.Core
{
    internal class Lexer
    {

        private IList<TokenPattern> _tokens;

        internal Lexer()
        {
            _tokens = new List<TokenPattern>();
            _tokens.Add(new TokenPattern { TokenType = TokenType.Comment, Pattern = "^\\;.*$" });
            _tokens.Add(new TokenPattern { TokenType = TokenType.Number, Pattern = "^((\\d+\\.\\d*)|(\\.\\d+)|(\\d+))(e(-|)\\d+|)"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.Variable, Pattern = "^(_+[a-zA-Z]+[a-zA-Z_\\d]*|[a-zA-Z]+[a-zA-Z_\\d]*)"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.Index, Pattern = "^!"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.Exponent, Pattern = "^\\^"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.SquareRoot, Pattern = "^#"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.Delta, Pattern = "^%"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.Root, Pattern = "^\""});
            _tokens.Add(new TokenPattern { TokenType = TokenType.Plus, Pattern = "^\\+"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.Minus, Pattern = "^-"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.Times, Pattern = "^(\\*|\\s)"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.Divide, Pattern = "^/"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.Differencial, Pattern = "^\\$"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.LeftParentheses, Pattern = "^\\("});
            _tokens.Add(new TokenPattern { TokenType = TokenType.RightParentheses, Pattern = "^\\)"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.BooleanEqual, Pattern = "^=="});
            _tokens.Add(new TokenPattern { TokenType = TokenType.BooleanLessEqual, Pattern = "^<="});
            _tokens.Add(new TokenPattern { TokenType = TokenType.BooleanMoreEqual, Pattern = "^>="});
            _tokens.Add(new TokenPattern { TokenType = TokenType.BooleanLess, Pattern = "^<"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.BooleanMore, Pattern = "^>"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.BooleanOr, Pattern = "^\\|"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.BooleanAnd, Pattern = "^\\&"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.Equal, Pattern = "^="});
            _tokens.Add(new TokenPattern { TokenType = TokenType.If, Pattern = "^\\,"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.Equations, Pattern = "^\\\\"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.Pi, Pattern = "^pi(?:\\b)"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.E, Pattern = "^e(?:\\b)"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.NaN, Pattern = "^NaN(?:\\b)"});
            _tokens.Add(new TokenPattern { TokenType = TokenType.Separator, Pattern = "^\\:"});
        }

        internal IList<TokenMatch> GetTokens(String text) 
        {
            int offset = 0;
            IList<TokenMatch> foundTokens = new List<TokenMatch>();
            while (text.Length > 0) 
            {
                bool found = false;
                foreach (TokenPattern tokenPattern in _tokens) 
                {
                 
                    var pattern = tokenPattern.Pattern;
                    var regex = new Regex(pattern);
                    if (regex.IsMatch(text)) {
                        var match = regex.Match(text);
                        found = true;
                        text = text.Substring(0, match.Index) + text.Substring(match.Index + match.Length);
                        foundTokens.Add(new TokenMatch
                        {
                            Type = tokenPattern.TokenType,
                            Text = match.Value,
                            Start = match.Index + offset,
                            End = match.Index + match.Length + offset
                        });
                        offset += match.Length;
                    }
                }
                if (!found) {
                    foundTokens.Add(new TokenMatch { Type = TokenType.Symbol, Text = text.Substring(0, 1), Start = offset, End = 1 + offset });
                    text = text.Substring(1, text.Length);
                    offset++;
                }
            }
            return foundTokens;
        }
    }
}
