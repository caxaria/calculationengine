﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CalculationEngine.Core.Parser
{
    internal class TokenPattern
    {
        internal TokenType TokenType { get; set; }
        internal String Pattern { get; set; }
    }
}
