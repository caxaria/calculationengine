﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

namespace CalculationEngine.Core.Tests
{
    [TestClass]
    public class LexerTests
    {
        Lexer lexer;

        [TestInitialize]
        public void Setup()
        {
            lexer = new Lexer();
        }

        [TestMethod]
        public void TestSimpleAddition()
        {
            var results = lexer.GetTokens("1+2");
            Assert.AreEqual(3, results.Count);
        }
    }
}
